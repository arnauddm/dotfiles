if status is-interactive
    # Commands to run in interactive sessions can go here
end

fish_add_path ~/.local/bin
fish_add_path ~/bin
fish_add_path /usr/local/bin
fish_add_path /usr/local/sbin
fish_add_path /usr/bin
fish_add_path /usr/sbin
fish_add_path ~/tools/flutter/bin
fish_add_path /home/arnauddematteis/tools/ideviceinstaller/src
fish_add_path ~/Android/Sdk/emulator
fish_add_path ~/Android/Sdk/platform-tools
fish_add_path ~/Android/Sdk/cmdline-tools/latest/bin
fish_add_path  ~/tools/gradle-7.4.2/bin

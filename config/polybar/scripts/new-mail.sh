#!/bin/bash

cd $HOME/.thunderbird
inbox=$(find . -name 'INBOX.msf')

#inbox=/home/arnauddematteis/.thunderbird/ewfmt4vt.default-release/ImapMail/outlook.office365.com/INBOX.msf

#list file in reverse, strip 94's, remove the first occurrence of 94=0 and all after
count=$(tac $inbox | grep '(^94=' | sed -n '/94=0/q;p')

#count line number for new emails
echo $count | grep -o "94=" | wc -l

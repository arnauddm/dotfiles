#!/bin/bash

for f in $(find config -mindepth 1 -maxdepth 1)
do
    rm -rf ~/.config/$(basename $f)
    cp -rf $f ~/.config/.
done
